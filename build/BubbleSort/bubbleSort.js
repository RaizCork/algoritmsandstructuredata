"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function bubbleSort(arr) {
    let newArr = [...arr];
    for (let i = 0; i < newArr.length; i++) {
        for (let j = 0; j < newArr.length - 1; j++) {
            if (newArr[j] > newArr[j + 1]) {
                let swap = newArr[j];
                newArr[j] = newArr[j + 1];
                newArr[j + 1] = swap;
            }
        }
    }
    return newArr;
}
exports.bubbleSort = bubbleSort;
