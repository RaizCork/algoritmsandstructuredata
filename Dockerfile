FROM node:10
WORKDIR /usr/app/AlgoritmsAndStructureData

COPY package.json .

RUN npm install

COPY src/ ./src
COPY build.num .
COPY tsconfig.json .

RUN ./node_modules/.bin/tsc
RUN rm ./tsconfig.json && rm -rf ./src

CMD [ "node", "./build"]
